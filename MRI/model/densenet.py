'''

I borrowed many parts of this from:
 https://github.com/kenshohara/video-classification-3d-cnn-pytorch/blob/master/models/densenet.py
@article{hara3dcnns,
  author={Kensho Hara and Hirokatsu Kataoka and Yutaka Satoh},
  title={Can Spatiotemporal 3D CNNs Retrace the History of 2D CNNs and ImageNet?},
  journal={arXiv preprint},
  volume={arXiv:1711.09577},
  year={2017},
}
'''

import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict
import math

__all__ = ['DenseNet', 'densenet121', 'densenet169', 'densenet201', 'densenet264']


def densenet121(**kwargs):
    model = DenseNet(num_init_features=16, growth_rate=32, block_config=(6, 12, 24, 16),
                     **kwargs)
    return model


def densenet169(**kwargs):
    model = DenseNet(num_init_features=64, growth_rate=32, block_config=(6, 12, 32, 32),
                     **kwargs)
    return model


def densenet201(**kwargs):
    model = DenseNet(num_init_features=64, growth_rate=32, block_config=(6, 12, 48, 32),
                     **kwargs)
    return model


def densenet264(**kwargs):
    model = DenseNet(num_init_features=64, growth_rate=32, block_config=(6, 12, 64, 48),
                     **kwargs)
    return model


# def get_fine_tuning_parameters(model, ft_begin_index):
#     if ft_begin_index == 0:
#         return model.parameters()
#
#     ft_module_names = []
#     for i in range(ft_begin_index, 5):
#         ft_module_names.append('denseblock{}'.format(ft_begin_index))
#         ft_module_names.append('transition{}'.format(ft_begin_index))
#     ft_module_names.append('norm5')
#     ft_module_names.append('classifier')
#
#     parameters = []
#     for k, v in model.named_parameters():
#         for ft_module in ft_module_names:
#             if ft_module in k:
#                 parameters.append({'params': v})
#                 break
#         else:
#             parameters.append({'params': v, 'lr': 0.0})
#
#     return parameters


class _DenseLayer(nn.Sequential):
    def __init__(self, num_input_features, growth_rate, bn_size, drop_rate):
        super(_DenseLayer, self).__init__()
        self.add_module('norm_1', nn.BatchNorm3d(num_input_features, eps=1e-01))
        self.add_module('relu_1', nn.LeakyReLU(inplace=True))
        self.add_module('conv_1', 
                        nn.Conv3d(
                            num_input_features, 
                            bn_size * growth_rate,
                            kernel_size=1, 
                            stride=1, 
                            bias=False))
        self.add_module('norm_2', nn.BatchNorm3d(bn_size * growth_rate, eps=1e-01))
        self.add_module('relu_2', nn.LeakyReLU(inplace=True))
        self.add_module('conv_2', 
                        nn.Conv3d(
                            bn_size * growth_rate, 
                            growth_rate,
                            kernel_size=3, 
                            stride=1, 
                            padding=1, 
                            bias=False))
        self.drop_rate = drop_rate

    def forward(self, x):
        new_features = super(_DenseLayer, self).forward(x)
        if self.drop_rate > 0:
            new_features = F.dropout(
                new_features, p=self.drop_rate, training=self.training)
        return torch.cat([x, new_features], 1)


class _DenseBlock(nn.Sequential):
    def __init__(self, num_layers, num_input_features, bn_size, growth_rate, drop_rate):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(num_input_features + i * growth_rate, 
                                growth_rate, 
                                bn_size, 
                                drop_rate)
            self.add_module('denselayer%d' % (i + 1), layer)


class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features):
        super(_Transition, self).__init__()
        self.add_module('norm', nn.BatchNorm3d(num_input_features, eps=1e-01))
        self.add_module('relu', nn.LeakyReLU(inplace=True))
        self.add_module('conv', 
                        nn.Conv3d(
                            num_input_features, 
                            num_output_features,
                            kernel_size=1, 
                            stride=1, 
                            bias=False))
        self.add_module('pool', nn.AvgPool3d(kernel_size=2, stride=2))


class DenseNet(nn.Module):
    """Densenet-BC model class
    Args:
        growth_rate (int) - how many filters to add each layer (k in paper)
        block_config (list of 4 ints) - how many layers in each pooling block
        num_init_features (int) - the number of filters to learn in the first convolution layer
        bn_size (int) - multiplicative factor for number of bottle neck layers
          (i.e. bn_size * k features in the bottleneck layer)
        drop_rate (float) - dropout rate after each dense layer
        num_classes (int) - number of classification classes
    """
    def __init__(self, 
                 growth_rate=32, 
                 block_config=(6, 12, 24, 16),
                 num_init_features=16, 
                 bn_size=4, 
                 drop_rate=0, 
                 num_classes=4, 
                 y_range=None):

        super(DenseNet, self).__init__()
        self.y_range = y_range
        self.num_classes = num_classes

        # First convolution
        self.features = nn.Sequential(OrderedDict([
            ('conv0', nn.Conv3d(1, 
                                num_init_features, 
                                kernel_size=3,
                                stride=(1, 2, 2), 
                                padding=3, 
                                bias=False)),
            ('norm0', nn.BatchNorm3d(num_init_features, eps=1e-01)),
            ('relu0', nn.LeakyReLU(inplace=True)),
            ('pool0', nn.MaxPool3d(kernel_size=3, stride=2, padding=1)),
        ]))

        # Each denseblock
        num_features = num_init_features
        for i, num_layers in enumerate(block_config):
            block = _DenseBlock(num_layers=num_layers,
                                num_input_features=num_features,
                                bn_size=bn_size,
                                growth_rate=growth_rate,
                                drop_rate=drop_rate)
            self.features.add_module('denseblock%d' % (i + 1), block)
            num_features = num_features + num_layers * growth_rate
            if i != len(block_config) - 1:
                trans = _Transition(num_input_features=num_features,
                                    num_output_features=num_features // 2)
                self.features.add_module('transition%d' % (i + 1), trans)
                num_features = num_features // 2

            # print(num_features)

        # Final batch norm
        self.features.add_module('norm5', nn.BatchNorm3d(num_features, eps=1e-01))

        # Linear layer

        self.classifier = nn.Linear(num_features, self.num_classes)

    def forward(self, x):
        features = self.features(x)
        out = F.relu(features, inplace=True)

        # last_duration = math.ceil(self.sample_duration / 16)
        # last_size = math.floor(self.sample_size / 32)
        # out = F.avg_pool3d(out, kernel_size=(last_duration, last_size, last_size)).view(features.size(0), -1)
        # if self.last_fc:

        bs, c, d, h, w = out.size()
        out = F.avg_pool3d(out, kernel_size=(d, h, w), stride=1)
        out = out.view(-1, features.size(1))
        out = self.classifier(out)
        
        if self.y_range is not None:
            out = (self.y_range[1]-self.y_range[0]) * torch.sigmoid(out) + self.y_range[0]
        
        
        return out