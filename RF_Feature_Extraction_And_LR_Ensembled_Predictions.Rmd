---
title: "RF_feature_extraction_and_LR_ensemble"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r}
#load the data, convert into train, val and test sets, recode columns as factors as needed
setwd('/data/bigbone/atolpadi/Knee_TKR_Prediction_Submission')# change as you need to
tkr_raw <- read.csv(file.path('MRI/labels/TKR_labels_with_image_predictions.csv'))
colnames(tkr_raw)

#recode categorical variables as factors - which variables are categorical was determined directly from OAI database
cat_cols = c(1,2,3,4,5,6,7,10,11,15,16,22,23,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48)

for (i in c(1:length(cat_cols))){
  tkr_raw[,cat_cols[i]] = as.factor(tkr_raw[,cat_cols[i]])
}
train_set = tkr_raw[tkr_raw$set == 0,]
train_lab = train_set$TKR_in_5_years
train_set = subset(train_set,select = -c(ID_Side_Visit,TKR_in_5_years,OA_status,Inferred_KLG,set,File_Path,dess_path,Image_Preds_MRI,Image_Preds_XRay))
val_set   = tkr_raw[tkr_raw$set == 1,]
val_set   = unique(val_set[1:nrow(val_set),])
val_lab   = val_set$TKR_in_5_years
val_set   = subset(val_set,select = -c(ID_Side_Visit,TKR_in_5_years,OA_status,Inferred_KLG,set,File_Path,dess_path,Image_Preds_MRI,Image_Preds_XRay))
test_set  = tkr_raw[tkr_raw$set == 2,]
test_set  = unique(test_set[1:nrow(test_set),])
test_lab  = test_set$TKR_in_5_years
test_set  = subset(test_set,select = -c(ID_Side_Visit,TKR_in_5_years,OA_status,Inferred_KLG,set,File_Path,dess_path,Image_Preds_MRI,Image_Preds_XRay))

```

## Including Plots

You can also embed plots, for example:

```{r}
#build your random forest 100 trees, and otherwise we're using all the default parameters for the random forest
library(randomForest)
require(caTools)
ntree = 100
set.seed(0)
rf <- randomForest(x = train_set,y = train_lab,xtest = val_set,ytest = val_lab,ntree = ntree,keep.forest = TRUE)
```

```{r}
# Iterate through each tree within the forest, and find within that tree the earliest node at which each feature was used for a split. By iterating through all trees, you update this list to find the earliest node depth at which every feature was used across the forest. Features whose minimum node depth is below the average minimum node depth of all features will be kept in a "trimmed" dataset

full_entries_track = data.frame(matrix(ncol = dim(train_set)[2],nrow = ntree))
best_in_each_track = data.frame(matrix(ncol = dim(train_set)[2],nrow = 1))
best_in_each_track[1,] = 1e10
colnames(best_in_each_track) = colnames(train_set)
colnames(full_entries_track) = colnames(train_set)
tree_depth = numeric(ntree)

for (j in c(1:ntree)){
  curr_entry_track = data.frame(matrix(ncol = dim(train_set)[2],nrow = 1))
  colnames(curr_entry_track) = colnames(train_set)
  curr_entry_track[1,] = 1e10
  
  onetree <- getTree(rf,j,labelVar = TRUE)
  onetree$depth <- NaN
  onetree$depth[1] <- 1
  for (i in c(1:(dim(onetree)[1]))){
    if (onetree$`left daughter`[i] != 0){
      left  = onetree$`left daughter`[i]
      right = onetree$`right daughter`[i]
      onetree$depth[left] = onetree$depth[i] + 1
      onetree$depth[right] = onetree$depth[i] + 1
    }
  }
  for (i in c(1:(dim(onetree)[1]))){
    if (!is.na(onetree$'split var'[i])){
      split_var_name = as.character(onetree$'split var'[i])
      curr_entry_track[,split_var_name] = min(curr_entry_track[,split_var_name],onetree$depth[i])
      best_in_each_track[,split_var_name] = min(best_in_each_track[,split_var_name],onetree$depth[i])
    }
  }
  full_entries_track[j,] = curr_entry_track
  curr_tree_depth <- max(onetree$depth)
  tree_depth[j] = curr_tree_depth
}
```


```{r}
# This function -- given a random seed, a trimmed version of the entire dataset on which only relevant features from the dataset are fed and duplicate entries are removed, a subset of the trimmed entire dataset that contains only the trimmed validation dataset, and the validation labels -- conducts a hyperparameter search that integrates imaging and non-imaging variables through logistic regression while optimizing weights of the 2 classes in the LR model. An array of the tested class weights and key model performance metrics is returned

hyperparameter_search <- function(random_seed,tkr_trimmed,val_set_trimmed,curr_lab){
  n_iterations = 100# search is for 100 iterations
  param_track = data.frame(matrix(ncol = 6, nrow = n_iterations))# array to be returned
  set.seed(random_seed)
  no_OA  = c(0,1)# these are the KL grade values associated with no OA; used later on
  mod_OA = c(2,3)
  sev_OA = c(4)
  temp_data <- subset(tkr_trimmed[tkr_trimmed$set == 0,],select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path)) # isolate the training data
  
  for (j in c(1:n_iterations)){
    # randomize the class weights and train a LR model that uses these class weights
    cw = numeric(dim(temp_data)[1])
    cw1 = 5*runif(1)
    cw2 = 5*runif(1)
    cw[temp_data$TKR_in_5_years == 0] = cw1
    cw[temp_data$TKR_in_5_years == 1] = cw2
    model <- glm(TKR_in_5_years ~.,family=binomial(link='logit'),data=temp_data,weights = cw)
    
    # further trim the validation set to just the predictor variables and get predictions on the validation set
    temp_data_val <- subset(val_set_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds <- predict(model,newdata =temp_data_val,type = "response")
    
    # calculate sensitivity, specificity, and accuracy for the given set of LR hyperparameters at no OA, moderate OA, and severe OA
    sens = c(0,0)
    spec = c(0,0)
    acc  = c(0,0)
    for (i in c(1:length(no_OA))){
      temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == no_OA[i],]
      temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == no_OA[i]]
      temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == no_OA[i]]
      sens[1] = sens[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
      sens[2] = sens[2] + length(temp_lab[temp_lab == 1])
      spec[1] = spec[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
      spec[2] = spec[2] + length(temp_lab[temp_lab == 0])
      acc[1] = acc[1] + sum(temp_lab == round(temp_pred))
      acc[2] = acc[2] + length(temp_lab == round(temp_pred))
    }
  
    sens_mod = c(0,0)
    spec_mod = c(0,0)
    acc_mod  = c(0,0)
    for (i in c(1:length(mod_OA))){
      temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == mod_OA[i],]
      temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == mod_OA[i]]
      temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == mod_OA[i]]
      sens_mod[1] = sens_mod[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
      sens_mod[2] = sens_mod[2] + length(temp_lab[temp_lab == 1])
      spec_mod[1] = spec_mod[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
      spec_mod[2] = spec_mod[2] + length(temp_lab[temp_lab == 0])
      acc_mod[1] = acc_mod[1] + sum(temp_lab == round(temp_pred))
      acc_mod[2] = acc_mod[2] + length(temp_lab == round(temp_pred))
    }
    
    sens_sev = c(0,0)
    spec_sev = c(0,0)
    acc_sev  = c(0,0)
    for (i in c(1:length(sev_OA))){
      temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == sev_OA[i],]
      temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == sev_OA[i]]
      temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == sev_OA[i]]
      sens_sev[1] = sens_sev[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
      sens_sev[2] = sens_sev[2] + length(temp_lab[temp_lab == 1])
      spec_sev[1] = spec_sev[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
      spec_sev[2] = spec_sev[2] + length(temp_lab[temp_lab == 0])
      acc_sev[1] = acc_sev[1] + sum(temp_lab == round(temp_pred))
      acc_sev[2] = acc_sev[2] + length(temp_lab == round(temp_pred))
    }
  
    # calculate AUC of model from the given hyperparameters
    rocobj <- roc(curr_lab,val_preds)
  
    param_track[j,] = c(cw1,cw2,sens[1]/sens[2]+spec[1]/spec[2],sens_mod[1]/sens_mod[2]+spec_mod[1]/spec_mod[2],
                        sens_sev[1]/sens_sev[2]+spec_sev[1]/spec_sev[2],auc(rocobj))
  }
  return (param_track)
}
```

Use hyperparameter search to aggregate predictions - MRI Model
```{r}
# This function returns a list of a performance table of the model, stratified by Oa status, and the predictions using the ensemble of models on the entire dataset. Input variables: num_ensembled = number of top models selected by Youden's index in each OA category, whose predictions are averaged to yield final LR prediction. param_track = output of hyperparameter search function. tkr_trimmed, val_set_trimmed, and curr_lab are same as previously defined

ensembled_predictor <- function(num_ensembled,param_track,tkr_trimmed,val_set_trimmed,curr_lab){
  # Isolate the top num_ensembled number of hyperparameter combinations from the param_track array
  best_no_OA  = as.numeric(rownames(param_track[order(param_track$X3,decreasing = TRUE),]))[1:num_ensembled]
  best_mod_OA = as.numeric(rownames(param_track[order(param_track$X4,decreasing = TRUE),]))[1:num_ensembled]
  best_sev_OA = as.numeric(rownames(param_track[order(param_track$X5,decreasing = TRUE),]))[1:num_ensembled]
  
  # Extract just the predictors from the tkr_trimmed array
  temp_data <- subset(tkr_trimmed[tkr_trimmed$set == 0,],select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
  
  # These have the same purpose as the hyperparameter search function
  no_OA  = c(0,1)
  mod_OA = c(2,3)
  sev_OA = c(4)
  
  #no OA model
  val_preds_no_OA_track = numeric(dim(val_set_trimmed)[1])# This is used to track performance just on the validation set - used to fill out a performance table of the validation set that can be displayed
  val_preds_no_OA_track_full = numeric(dim(val_set_trimmed)[1])# This is used to obtain predictions on the entire dataset - used to return final ensembled predictions
  for(i in c(1:num_ensembled)){
    cw = numeric(dim(temp_data)[1])
    cw1 = param_track[best_no_OA[i],1]
    cw2 = param_track[best_no_OA[i],2]
    cw[temp_data$TKR_in_5_years == 0] = cw1
    cw[temp_data$TKR_in_5_years == 1] = cw2
    model <- glm(TKR_in_5_years ~.,family=binomial(link='logit'),data=temp_data,weights = cw)
    
    temp_data_val <- subset(val_set_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_no_OA <- predict(model,newdata =temp_data_val,type = "response")
    
    temp_data_val <- subset(tkr_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_no_OA_full <- predict(model,newdata =temp_data_val,type = "response")
   val_preds_no_OA_track <- val_preds_no_OA_track + val_preds_no_OA
   val_preds_no_OA_track_full <- val_preds_no_OA_track_full + val_preds_no_OA_full
  }
  val_preds_no_OA <- val_preds_no_OA_track/num_ensembled
  val_preds_no_OA_full <- val_preds_no_OA_track_full/num_ensembled
  
  #same, but for moderate OA
  val_preds_mod_OA_track = numeric(dim(val_set_trimmed)[1])
  val_preds_mod_OA_track_full = numeric(dim(val_set_trimmed)[1])
  for(i in c(1:num_ensembled)){
    cw = numeric(dim(temp_data)[1])
    cw1 = param_track[best_mod_OA[i],1]
    cw2 = param_track[best_mod_OA[i],2]
    cw[temp_data$TKR_in_5_years == 0] = cw1
    cw[temp_data$TKR_in_5_years == 1] = cw2
    model <- glm(TKR_in_5_years ~.,family=binomial(link='logit'),data=temp_data,weights = cw)
    
    temp_data_val <- subset(val_set_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_mod_OA <- predict(model,newdata =temp_data_val,type = "response")
    
    temp_data_val <- subset(tkr_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_mod_OA_full <- predict(model,newdata =temp_data_val,type = "response")
    val_preds_mod_OA_track <- val_preds_mod_OA_track + val_preds_mod_OA
    val_preds_mod_OA_track_full <- val_preds_mod_OA_track_full + val_preds_mod_OA_full
  }
  val_preds_mod_OA <- val_preds_mod_OA_track/num_ensembled
  val_preds_mod_OA_full <- val_preds_mod_OA_track_full/num_ensembled
  
  #same, but for severe OA
  val_preds_sev_OA_track = numeric(dim(val_set_trimmed)[1])
  val_preds_sev_OA_track_full = numeric(dim(val_set_trimmed)[1])
  for (i in c(1:num_ensembled)){
    cw = numeric(dim(temp_data)[1])
    cw1 = param_track[best_sev_OA[i],1]
    cw2 = param_track[best_sev_OA[i],2]
    cw[temp_data$TKR_in_5_years == 0] = cw1
    cw[temp_data$TKR_in_5_years == 1] = cw2
    model <- glm(TKR_in_5_years ~.,family=binomial(link='logit'),data=temp_data,weights = cw)
    
    temp_data_val <- subset(val_set_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_sev_OA <- predict(model,newdata =temp_data_val,type = "response")
    
    temp_data_val <- subset(tkr_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
    val_preds_sev_OA_full <- predict(model,newdata =temp_data_val,type = "response")
    val_preds_sev_OA_track <- val_preds_sev_OA_track + val_preds_sev_OA
    val_preds_sev_OA_track_full <- val_preds_sev_OA_track_full + val_preds_sev_OA_full
  }
  val_preds_sev_OA <- val_preds_sev_OA_track/num_ensembled
  val_preds_sev_OA_full <- val_preds_sev_OA_track_full/num_ensembled
  
  # Fill out predictions for the entire dataset to return as a vector of LR ensembled predictions
  val_preds_full = numeric(length(val_preds_no_OA))
  val_preds_full[tkr_trimmed$Inferred_KLG == 0] = val_preds_no_OA_full[tkr_trimmed$Inferred_KLG == 0]
  val_preds_full[tkr_trimmed$Inferred_KLG == 1] = val_preds_no_OA_full[tkr_trimmed$Inferred_KLG == 1]
  val_preds_full[tkr_trimmed$Inferred_KLG == 2] = val_preds_mod_OA_full[tkr_trimmed$Inferred_KLG == 2]
  val_preds_full[tkr_trimmed$Inferred_KLG == 3] = val_preds_mod_OA_full[tkr_trimmed$Inferred_KLG == 3]
  val_preds_full[tkr_trimmed$Inferred_KLG == 4] = val_preds_sev_OA_full[tkr_trimmed$Inferred_KLG == 4]

  # Fill out predictions just for the validation set to use to calculate a performance table on the validation data
  temp_data <- subset(tkr_trimmed[tkr_trimmed$set == 0,],select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
  val_preds = numeric(length(val_preds_no_OA))
  val_preds[val_set_trimmed$Inferred_KLG == 0] = val_preds_no_OA[val_set_trimmed$Inferred_KLG == 0]
  val_preds[val_set_trimmed$Inferred_KLG == 1] = val_preds_no_OA[val_set_trimmed$Inferred_KLG == 1]
  val_preds[val_set_trimmed$Inferred_KLG == 2] = val_preds_mod_OA[val_set_trimmed$Inferred_KLG == 2]
  val_preds[val_set_trimmed$Inferred_KLG == 3] = val_preds_mod_OA[val_set_trimmed$Inferred_KLG == 3]
  val_preds[val_set_trimmed$Inferred_KLG == 4] = val_preds_sev_OA[val_set_trimmed$Inferred_KLG == 4]
    
  # Calculate sensitivity, specificity, and accuracy in each OA classification and overall
  sens = c(0,0)
  spec = c(0,0)
  acc  = c(0,0)
  for (i in c(1:length(no_OA))){
    temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == no_OA[i],]
    temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == no_OA[i]]
    temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == no_OA[i]]
    sens[1] = sens[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
    sens[2] = sens[2] + length(temp_lab[temp_lab == 1])
    spec[1] = spec[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
    spec[2] = spec[2] + length(temp_lab[temp_lab == 0])
    acc[1] = acc[1] + sum(temp_lab == round(temp_pred))
    acc[2] = acc[2] + length(temp_lab == round(temp_pred))
  }
  
  # Moderate OA metrics
  sens_mod = c(0,0)
  spec_mod = c(0,0)
  acc_mod  = c(0,0)
  for (i in c(1:length(mod_OA))){
    temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == mod_OA[i],]
    temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == mod_OA[i]]
    temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == mod_OA[i]]
    sens_mod[1] = sens_mod[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
    sens_mod[2] = sens_mod[2] + length(temp_lab[temp_lab == 1])
    spec_mod[1] = spec_mod[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
    spec_mod[2] = spec_mod[2] + length(temp_lab[temp_lab == 0])
    acc_mod[1] = acc_mod[1] + sum(temp_lab == round(temp_pred))
    acc_mod[2] = acc_mod[2] + length(temp_lab == round(temp_pred))
  }
  
  # Severe OA metrics
  sens_sev = c(0,0)
  spec_sev = c(0,0)
  acc_sev  = c(0,0)
  for (i in c(1:length(sev_OA))){
    temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == sev_OA[i],]
    temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == sev_OA[i]]
    temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == sev_OA[i]]
    sens_sev[1] = sens_sev[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
    sens_sev[2] = sens_sev[2] + length(temp_lab[temp_lab == 1])
    spec_sev[1] = spec_sev[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
    spec_sev[2] = spec_sev[2] + length(temp_lab[temp_lab == 0])
    acc_sev[1] = acc_sev[1] + sum(temp_lab == round(temp_pred))
    acc_sev[2] = acc_sev[2] + length(temp_lab == round(temp_pred))
  }
  
  # Calculate performance table
  mat <- data.frame(matrix(ncol = 4,nrow = 4))
  mat[1,] <- c(sens[1]/sens[2],spec[1]/spec[2],acc[1]/acc[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 0 | val_set_trimmed$Inferred_KLG == 1)], val_preds[which(val_set_trimmed$Inferred_KLG == 0 | val_set_trimmed$Inferred_KLG == 1)])))
  mat[2,] <- c(sens_mod[1]/sens_mod[2],spec_mod[1]/spec_mod[2],acc_mod[1]/acc_mod[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 2 | val_set_trimmed$Inferred_KLG == 3)], val_preds[which(val_set_trimmed$Inferred_KLG == 2 | val_set_trimmed$Inferred_KLG == 3)])))
  mat[3,] <- c(sens_sev[1]/sens_sev[2],spec_sev[1]/spec_sev[2],acc_sev[1]/acc_sev[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 4)], val_preds[which(val_set_trimmed$Inferred_KLG == 4)])))
  mat[4,] <- c(sum(curr_lab[curr_lab == 1] == round(val_preds[curr_lab == 1]))/length(curr_lab[curr_lab == 1]),sum(curr_lab[curr_lab == 0] == round(val_preds[curr_lab == 0]))/length(curr_lab[curr_lab == 0]),sum(curr_lab == round(val_preds))/length(curr_lab),auc(roc(curr_lab,val_preds)))
  colnames(mat) <- c('Sensitivity','Specificity','Accuracy','AUC')
  rownames(mat) <- c('No_OA','Mod_OA','Sev_OA','All')
  hybrid_mat <- mat
  
  #Return full vector of predictions and validation performance table
  return (list(val_preds_full,hybrid_mat))
}
```

```{r}
# Use validation set for selection of ideal hyperparameters to integrate imaging and non-imaging predictions
curr_lab <- val_lab
curr_set_num <- 1
```

```{r}
# Use extracted relevant features and MRI image predictions to run a hyperparameter search, get ensembled predictions, display performance table for validation data, and get full vector of predictions of the resulting ensemble

# inplement feature extraction
mean_min_depth <- mean(as.numeric(best_in_each_track))
selector <- data.frame(matrix(ncol = dim(tkr_raw)[2],nrow = 1))
colnames(selector) <- colnames(tkr_raw)
selector[1,1:(dim(tkr_raw)[2])] = TRUE
selector[1,8:(dim(tkr_raw)[2]-2)] <- (best_in_each_track < mean_min_depth)
selector[1,(dim(tkr_raw)[2]-2):dim(tkr_raw)[2]] <- FALSE
selector[1,(dim(tkr_raw)[2]-1)] <- TRUE

train_set_trimmed <- train_set[,best_in_each_track < mean_min_depth]
val_set_trimmed   <- val_set[,best_in_each_track < mean_min_depth]
test_set_trimmed  <- test_set[,best_in_each_track < mean_min_depth]

tkr_trimmed = tkr_raw[,unlist(selector)]
val_set_trimmed = tkr_trimmed[tkr_trimmed$set == curr_set_num,]
val_set_trimmed = unique(val_set_trimmed[1:nrow(val_set_trimmed),])

param_track <- hyperparameter_search(28,tkr_trimmed,val_set_trimmed,curr_lab)
ensembled_return <- ensembled_predictor(4,param_track,tkr_trimmed,val_set_trimmed,curr_lab)
MRI_hybrid_preds <- ensembled_return[[1]]
hybrid_mat <- ensembled_return[[2]]
hybrid_mat

```



```{r}
library(pROC)
# Run the same thing as above, but for X-ray images

mean_min_depth <- mean(as.numeric(best_in_each_track))
selector <- data.frame(matrix(ncol = dim(tkr_raw)[2],nrow = 1))
colnames(selector) <- colnames(tkr_raw)
selector[1,1:(dim(tkr_raw)[2])] = TRUE
selector[1,8:(dim(tkr_raw)[2]-2)] <- (best_in_each_track < mean_min_depth)
selector[1,(dim(tkr_raw)[2]-2):dim(tkr_raw)[2]] <- FALSE
selector[1,(dim(tkr_raw)[2])] <- TRUE

train_set_trimmed <- train_set[,best_in_each_track < mean_min_depth]
val_set_trimmed   <- val_set[,best_in_each_track < mean_min_depth]
test_set_trimmed  <- test_set[,best_in_each_track < mean_min_depth]

tkr_trimmed = tkr_raw[,unlist(selector)]
val_set_trimmed = tkr_trimmed[tkr_trimmed$set == curr_set_num,]
val_set_trimmed = unique(val_set_trimmed[1:nrow(val_set_trimmed),])


param_track_x <- hyperparameter_search(14,tkr_trimmed,val_set_trimmed,curr_lab)
ensembled_return <- ensembled_predictor(5,param_track_x,tkr_trimmed,val_set_trimmed,curr_lab)
XRay_hybrid_preds <- ensembled_return[[1]]
hybrid_mat <- ensembled_return[[2]]
hybrid_mat

```


```{r}
# This code chunk trains a LR model that predicts TKR onset within 5 years using solely the non-imaging variables. It returns both a performance table on the validation data and a vector of the ensembled LR predictions for all points within the original dataset

# Feature extraction
library(pROC)

mean_min_depth <- mean(as.numeric(best_in_each_track))
selector <- data.frame(matrix(ncol = dim(tkr_raw)[2],nrow = 1))
colnames(selector) <- colnames(tkr_raw)
selector[1,1:(dim(tkr_raw)[2])] = TRUE
selector[1,8:(dim(tkr_raw)[2]-2)] <- (best_in_each_track < mean_min_depth)
selector[1,(dim(tkr_raw)[2]-2):dim(tkr_raw)[2]] <- FALSE

train_set_trimmed <- train_set[,best_in_each_track < mean_min_depth]
val_set_trimmed   <- val_set[,best_in_each_track < mean_min_depth]
test_set_trimmed  <- test_set[,best_in_each_track < mean_min_depth]

tkr_trimmed = tkr_raw[,unlist(selector)]
val_set_trimmed = tkr_trimmed[tkr_trimmed$set == curr_set_num,]
val_set_trimmed = unique(val_set_trimmed[1:nrow(val_set_trimmed),])

# Extract solely the predictor variables from the trimmed datasets, for both the overall datasets and the validation data. Train a model to predidct TKR from the non-imaging variables and run it on the validation data
set.seed(28)
temp_data <- subset(tkr_trimmed[tkr_trimmed$set == 0,],select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
model <- glm(TKR_in_5_years ~.,family=binomial(link='logit'),data=temp_data)

temp_data_val <- subset(val_set_trimmed,select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
val_preds <- predict(model,newdata =temp_data_val,type = "response")

# Carry out all the performance table calculations for validation data
no_OA  = c(0,1)
mod_OA = c(2,3)
sev_OA = c(4)

sens = c(0,0)
spec = c(0,0)
acc  = c(0,0)
for (i in c(1:length(no_OA))){
  temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == no_OA[i],]
  temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == no_OA[i]]
  temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == no_OA[i]]
  sens[1] = sens[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
  sens[2] = sens[2] + length(temp_lab[temp_lab == 1])
  spec[1] = spec[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
  spec[2] = spec[2] + length(temp_lab[temp_lab == 0])
  acc[1] = acc[1] + sum(temp_lab == round(temp_pred))
  acc[2] = acc[2] + length(temp_lab == round(temp_pred))
}

sens_mod = c(0,0)
spec_mod = c(0,0)
acc_mod  = c(0,0)
for (i in c(1:length(mod_OA))){
  temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == mod_OA[i],]
  temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == mod_OA[i]]
  temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == mod_OA[i]]
  sens_mod[1] = sens_mod[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
  sens_mod[2] = sens_mod[2] + length(temp_lab[temp_lab == 1])
  spec_mod[1] = spec_mod[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
  spec_mod[2] = spec_mod[2] + length(temp_lab[temp_lab == 0])
  acc_mod[1] = acc_mod[1] + sum(temp_lab == round(temp_pred))
  acc_mod[2] = acc_mod[2] + length(temp_lab == round(temp_pred))
}

sens_sev = c(0,0)
spec_sev = c(0,0)
acc_sev  = c(0,0)
for (i in c(1:length(sev_OA))){
  temp_set  <- val_set_trimmed[val_set_trimmed$Inferred_KLG == sev_OA[i],]
  temp_lab  <- curr_lab[val_set_trimmed$Inferred_KLG == sev_OA[i]]
  temp_pred <- val_preds[val_set_trimmed$Inferred_KLG == sev_OA[i]]
  sens_sev[1] = sens_sev[1] + sum(temp_lab[temp_lab == 1] == round(temp_pred[temp_lab == 1]))
  sens_sev[2] = sens_sev[2] + length(temp_lab[temp_lab == 1])
  spec_sev[1] = spec_sev[1] + sum(temp_lab[temp_lab == 0] == round(temp_pred[temp_lab == 0]))
  spec_sev[2] = spec_sev[2] + length(temp_lab[temp_lab == 0])
  acc_sev[1] = acc_sev[1] + sum(temp_lab == round(temp_pred))
  acc_sev[2] = acc_sev[2] + length(temp_lab == round(temp_pred))
}

# Calculate vector of predictions for entire dataset
clin_df_final <- subset(tkr_raw[,unlist(selector)],select = -c(ID_Side_Visit,OA_status,Inferred_KLG,set,File_Path,dess_path))
clin_preds_final <- predict(model,newdata =clin_df_final,type = "response")

# Consolidate performance metrics into a performance table and display
mat <- data.frame(matrix(ncol = 4,nrow = 4))
mat[1,] <- c(sens[1]/sens[2],spec[1]/spec[2],acc[1]/acc[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 0 | val_set_trimmed$Inferred_KLG == 1)], val_preds[which(val_set_trimmed$Inferred_KLG == 0 | val_set_trimmed$Inferred_KLG == 1)])))
mat[2,] <- c(sens_mod[1]/sens_mod[2],spec_mod[1]/spec_mod[2],acc_mod[1]/acc_mod[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 2 | val_set_trimmed$Inferred_KLG == 3)], val_preds[which(val_set_trimmed$Inferred_KLG == 2 | val_set_trimmed$Inferred_KLG == 3)])))
mat[3,] <- c(sens_sev[1]/sens_sev[2],spec_sev[1]/spec_sev[2],acc_sev[1]/acc_sev[2],auc(roc(curr_lab[which(val_set_trimmed$Inferred_KLG == 4)], val_preds[which(val_set_trimmed$Inferred_KLG == 4)])))
mat[4,] <- c(sum(curr_lab[curr_lab == 1] == round(val_preds[curr_lab == 1]))/length(curr_lab[curr_lab == 1]),sum(curr_lab[curr_lab == 0] == round(val_preds[curr_lab == 0]))/length(curr_lab[curr_lab == 0]),sum(curr_lab == round(val_preds))/length(curr_lab),auc(roc(curr_lab,val_preds)))
colnames(mat) <- c('Sensitivity','Specificity','Accuracy','AUC')
rownames(mat) <- c('No_OA','Mod_OA','Sev_OA','All')
clin_mat <- mat
clin_mat
```

```{r}
# Save the extracted features and the predictions of the 3 models--the ensembled MRI image and X-ray image models, as well as the non-imaging variable only models--into a single array and export as a csv file. Calculation of confidence intervals for ROC curves and performance tables, and statistical tests is carried out in Python codes.
selector <- data.frame(matrix(ncol = dim(tkr_raw)[2],nrow = 1))
colnames(selector) <- colnames(tkr_raw)
selector[1,1:(dim(tkr_raw)[2])] = TRUE
selector[1,8:(dim(tkr_raw)[2]-2)] <- (best_in_each_track < mean_min_depth)
selector[1,(dim(tkr_raw)[2]-2):dim(tkr_raw)[2]] <- TRUE
selector[1,(dim(tkr_raw)[2]-1)] <- TRUE
tkr_trimmed = tkr_raw[,unlist(selector)]
tkr_trimmed$Clin_Model <- clin_preds_final
tkr_trimmed$MRI_Hybrid_Model <- MRI_hybrid_preds
tkr_trimmed$XRay_Hybrid_Model <- XRay_hybrid_preds
write.csv(tkr_trimmed,file.path(getwd(),'MRI/labels/TKR_labels_all_pipeline_versions_2.csv'))
write.csv(tkr_trimmed,file.path(getwd(),'X-ray/labels/TKR_labels_all_pipeline_versions_2.csv'))
```