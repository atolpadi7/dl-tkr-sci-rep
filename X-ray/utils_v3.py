import json
import logging
import os
import numpy as np
import csv
import shutil
from sklearn.metrics import (accuracy_score, fbeta_score, precision_score,
                             recall_score, confusion_matrix,
                             explained_variance_score, r2_score,
                             mean_absolute_error, mean_squared_error)
import torch.nn.functional as F
import torch.nn as nn
import torch
import datetime


class RunningAverage():
    """A simple class that maintains the running average of a quantity

    Example:
    ```
    loss_avg = RunningAverage()
    loss_avg.update(2)
    loss_avg.update(4)
    loss_avg() = 3
    ```
    """

    def __init__(self):
        self.steps = 0
        self.total = 0

    def update(self, val):
        self.total += val
        self.steps += 1

    def __call__(self):
        return self.total / float(self.steps)


def set_logger(log_path):
    """Set the logger to log info in terminal and file `log_path`.
    In general, it is useful to have a logger so that every output to the terminal
    is saved in a permanent file. Here we save it to `model_dir/train.log`.
    Example:
    ```
    logging.info("Starting training...")
    ```
    Args:
        log_path: (string) where to log
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    if not logger.handlers:
        # Logging to a file
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
        logger.addHandler(file_handler)

        # Logging to console
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(stream_handler)


def save_dict_to_json(d, json_path):
    """Saves dict of floats in json file
    Args:
        d: (dict) of float-castable values (np.float, int, float, etc.)
        json_path: (string) path to json file
    """
    with open(json_path, 'w') as f:
        # We need to convert the values to float for json (it doesn't accept np.array, np.float, )
        d = {k: float(v) for k, v in d.items()}
        json.dump(d, f, indent=4)


def save_checkpoint(state, is_best, checkpoint):
    """Saves model and training parameters at checkpoint + 'last.pth.tar'. If is_best==True, also saves
    checkpoint + 'best.pth.tar'
    Args:
        state: (dict) contains model's state_dict, may contain other keys such as epoch, optimizer state_dict
        is_best: (bool) True if it is the best model seen till now
        checkpoint: (string) folder where parameters are to be saved
    """
    filepath = os.path.join(checkpoint, 'last.pth.tar')
    if not os.path.exists(checkpoint):
        print("Checkpoint Directory does not exist! Making directory {}".format(checkpoint))
        os.mkdir(checkpoint)
    else:
        print("Checkpoint Directory exists! ")   
    
    torch.save(state, filepath)
    
    if is_best:
        filename = 'best_{}.pth.tar'.format(str(datetime.datetime.now()))
        shutil.copyfile(filepath, os.path.join(checkpoint, filename))


def load_checkpoint(checkpoint, model, optimizer=None):
    """Loads model parameters (state_dict) from file_path. If optimizer is provided, loads state_dict of
    optimizer assuming it is present in checkpoint.
    Args:
        checkpoint: (string) filename which needs to be loaded
        model: (torch.nn.Module) model for which the parameters are loaded
        optimizer: (torch.optim) optional: resume optimizer from checkpoint
    """
    if not os.path.exists(checkpoint):
        raise("File doesn't exist {}".format(checkpoint))
    checkpoint = torch.load(checkpoint)
    model.load_state_dict(checkpoint['state_dict'])

    if optimizer:
        optimizer.load_state_dict(checkpoint['optim_dict'])

    return checkpoint


def loss_fn(outputs, labels):
    prob = nn.Softmax(outputs, 1) * 100
    labels = labels.float()
    return F.mse_loss(prob, labels)


def accuracy_v2(output, labels):
    preds = np.argmax(output, axis=1)
    true = labels
    return accuracy_score(y_true=true, y_pred=preds)

def precision(output, labels):
    return precision_score(y_true=labels,
                           y_pred=np.argmax(output, axis=1))

def recall(output, labels):
    return recall_score(y_true=labels,
                        y_pred=np.argmax(output, axis=1))

def fbeta(output, labels, beta=.5):
    return fbeta_score(y_true=labels,
                       y_pred=np.argmax(output, 1),
                       beta=beta)

def explained_variance(output, labels):
    '''
    Best possible score is 1.0, lower values are worse.
    '''
    return explained_variance_score(y_pred=output, y_true=labels)

def MAE(output, labels):
    return mean_absolute_error(y_pred=output, y_true=labels)

def r2(output, labels):
    return r2_score(y_pred=output, y_true=labels)

def log_csv(epoch, batch_idx, metrics, filename):
    log_file = open(filename, 'wb')
    log_fields = list(metrics.keys())
    log_writer = csv.DictWriter(log_file, filename=log_fields)
    log_writer.writeheader()


def specificity(output, labels):
    tmp = confusion_matrix(y_true=labels,
                           y_pred=np.argmax(output, axis=1)).ravel()
    if len(tmp) == 4:
        tn, fp, fn, tp = tmp[0], tmp[1], tmp[2], tmp[3]
        return tn / (tn + fp)

    else:
        return 0.


metrics = {
    'R2': r2,
    'mean_absolute_error':MAE,
    'explained_variance':explained_variance,
    'accuracy': accuracy_v2
}
